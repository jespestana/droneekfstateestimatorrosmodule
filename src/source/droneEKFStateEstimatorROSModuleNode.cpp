//////////////////////////////////////////////////////
//  droneEKFStateEstimatorROSModuleNode.cpp
//
//  Created on: Dec 11, 2013
//      Author: jespestana
//
//  Last modification on: Dec 11, 2013
//      Author: jespestana
//
//////////////////////////////////////////////////////

#include <iostream>
#include <math.h>

#include "ros/ros.h"
#include "droneEKFStateEstimatorROSModule.h"
#include "nodes_definition.h"

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    ros::NodeHandle n;

    std::cout << "[ROSNODE] Starting "<<ros::this_node::getName() << std::endl;

    DroneEKFStateEstimatorROSModule MyDroneEKFStateEstimator;
    MyDroneEKFStateEstimator.open(n);

    try
    {
        while(ros::ok())
        {
            //Read messages
            ros::spinOnce();

            //Run EKF State Estimator
            if(MyDroneEKFStateEstimator.run())
            {
            }
            //Sleep
            MyDroneEKFStateEstimator.sleep();
        }

    }
    catch (std::exception &ex)
    {
        std::cout << "[ROSNODE] Exception :" << ex.what() << std::endl;
    }
    return 0;
}
